# Poultry lib : a library of Unity3D utilities for the Poultry Game Jam team



## Index

[TOC]

## 1. Installing

This library is better used as a git submodule :

   `git submodule add https://gitlab.com/Panzerr/poultrylib.git`

## 2. Features

#### 2.1 poultry::Spawn a object instantiation management tool

Simplifies greatly the instantiation of new game objects and components by providing a single API to instantiate, initialize and check for uniqueness.

#### 2.2 poultry::Demo a script for demo scene

Sometimes you need to work on base features of your game but want to be able to see them work without making a full UI. The Demo script lets you call methods of a component in a configurable loop, so that you can see your code working.

#### 2.3 DragNDrop facilities

*Note : this feature works but is up for a review/redesign, use at your own risk!*

A DragNDrop target and a DragNDroppable components : add them to your GameoObjects to make your object dragNdroppable and be notified when an object is dragged.

#### 2.4 poultry::CanvasPixelScaler

A simple unity to rescale canvas for pixel art. Add it to your canvas in edit mode to scale it and then remove it afterwards as it is an Editor mode script.



