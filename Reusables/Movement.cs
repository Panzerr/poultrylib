﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry {
    public class Movement : MonoBehaviour
    {

        [SerializeField]
        private float speed = 1f;

        public float Speed {
            get { return speed; }
            set { speed = value; }
        }

        [SerializeField]
        private Animator animator;

        public void Move(Vector3 direction)
        {
            if (direction.magnitude > 1)
                direction.Normalize();

            if (animator)
            {
                animator.SetFloat("dir_x", direction.x);
                animator.SetFloat("dir_y", direction.y);
            }
            transform.Translate(direction * speed * Time.deltaTime);
        }
    }

}