﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry
{

    /* In a lot of games we can move the player using unity axes defined in the project setting, this components does that */
    [RequireComponent(typeof(Movement))]
    public class MoveWithAxis : MonoBehaviour
    {
        /* structure to add custom mappings for axes*/
        [System.Serializable]
        private struct AxisMapping
        {
            [SerializeField]
            public string axis;
            public Vector3 direction;
        }

        [SerializeField]
        AxisMapping[] mappings;

        private Movement movement;

        /* Set sensible default values */
        void Start()
        {
            if (mappings.Length == 0)
            {
                mappings = new AxisMapping[] {
                new AxisMapping{axis = "Horizontal", direction = new Vector3(1,0,0) },
                new AxisMapping{axis = "Vertical", direction = new Vector3(0,1,0) },
                };
            }
            movement = GetComponent<Movement>();
        }

        /* Use inputs to calculate movement */
        void Update()
        {
            Vector3 direction = new Vector3(0, 0, 0);
            foreach (AxisMapping mapping in mappings)
            {
                direction += Input.GetAxis(mapping.axis) * mapping.direction;
            }
            movement.Move(direction);
        }
    }
}