﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class CanvasPixelScaler : MonoBehaviour
{
    private RectTransform rectTransform;

    [SerializeField]
    Vector2 resolution = new Vector2(256,256);

    [SerializeField]
    float sizeX =1;

    [SerializeField]
    float sizeY =1;
    
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        rectTransform.sizeDelta = resolution;
        rectTransform.transform.localScale = new Vector3(sizeX / resolution.x, sizeY / resolution.y);
    }
}
