﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry {

    using Transition = System.Tuple<int, int, int>;

    /// <summary>
    /// Finite automaton
    /// </summary>
    public class Automaton
    {
        /// <summary>
        /// Delegate to register on state change
        /// </summary>
        public delegate void StateChangeDelegate();

        private class State
        {

            /// <summary>
            /// Identifier (we suppose tokens and states are to be enums
            /// </summary>
            private int identifier;

            public int Identifier
            {
                get { return identifier; }
                private set { identifier = value; }
            }

            /// <summary>
            /// Token -> next state transitions
            /// </summary>
            private Dictionary<int, State> transitions;

            /// <summary>
            /// list of delegates to call when entering this state
            /// </summary>
            private List<StateChangeDelegate> onStateChanged;

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="id">the identifier of the state</param>
            public State(int id)
            {
                identifier = id;
                transitions = new Dictionary<int, State>();
                onStateChanged = new List<StateChangeDelegate>();
            }

            /// <summary>
            /// Read a token and calls the statechange delegates on state change
            /// </summary>
            /// <param name="token">the token to read</param>
            /// <returns>the next transition</returns>
            public State ReadToken(int token)
            {
                if (transitions.ContainsKey(token))
                {
                    return transitions[token];
                }
                else
                {
                    return this;
                }
            }

            /// <summary>
            /// Call all the callbacks on entering this state
            /// </summary>
            public void Enter()
            {
                foreach (StateChangeDelegate stateChangeDelegate in onStateChanged)
                {
                    stateChangeDelegate();
                }
            }

            /// <summary>
            /// Register a callback to this state
            /// </summary>
            /// <param name="stateChangeDelegate">the callback to register</param>
            public void RegisterCallback(StateChangeDelegate stateChangeDelegate)
            {
                onStateChanged.Add(stateChangeDelegate);
            }

            /// <summary>
            /// Unregister a callback to this state
            /// </summary>
            /// <param name="stateChangeDelegate">the callback to unregister</param>
            public void UnRegisterCallback(StateChangeDelegate stateChangeDelegate)
            {
                onStateChanged.Remove(stateChangeDelegate);
            }

            /// <summary>
            /// Add a new transition from this state
            /// </summary>
            /// <param name="token">token to go to the target</param>
            /// <param name="target">the target state</param>
            public void AddTransition(int token, State target)
            {
                transitions[token] = target;
            }

        }

        private State currentState;

        /// <summary>
        /// Get the current state
        /// </summary>
        /// <returns>an int usually an enum representation</returns>
        public int GetCurrentState()
        {
            return currentState.Identifier;
        }

        private Dictionary<int, State> States;
        
        /// <summary>
        /// Create an automaton
        /// </summary>
        /// <param name="states">array of states</param>
        /// <param name="transitions">arrays of transitions : (start state, token red, transition) </param>
        public Automaton(int[] states, Transition[] transitions)
        {
            if (states == null || states.Length == 0)
                throw new System.Exception("Trying to create an empty automaton");

            States = new Dictionary<int, State>();
            foreach (int state in states)
            {
                States[state] = new State(state);
            }
            currentState = States[states[0]];
            foreach (System.Tuple<int,int,int> transition in transitions)
            {
                State target = States[transition.Item3];
                if ( target != null)
                {
                    States[transition.Item1]?.AddTransition(transition.Item2, target);
                }
            }
        }

        /// <summary>
        /// Register a function to be called on entering a certain state
        /// </summary>
        /// <param name="state">state to register to</param>
        /// <param name="callback">delegate to register</param>
        public void RegisterOnState(int state, StateChangeDelegate callback)
        {
            States[state]?.RegisterCallback(callback);
        }

        /// <summary>
        /// UnRegister a function to be called on entering a certain state
        /// </summary>
        /// <param name="state">state to unregister to</param>
        /// <param name="stateChangeDelegate">delegate to unregister</param>
        public void UnRegisterOnState(int state, StateChangeDelegate callback)
        {
            States[state]?.UnRegisterCallback(callback);
        }

        /// <summary>
        /// Read a token and move to the next state if necessary
        /// </summary>
        /// <param name="token">the token to read</param>
        public void ReadToken(int token)
        {
            currentState = currentState.ReadToken(token);
            currentState.Enter();
        }

    }
}