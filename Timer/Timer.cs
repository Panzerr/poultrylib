﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry
{
    /// <summary>
    /// Timer (does not run by itself)
    /// </summary>
    public class Timer
    {
        bool running = false;

        public bool Running { get { return running; } }

        float targetTime;

        float currentTime;

        public float Remaining
        {
            get {  return running ? targetTime - currentTime : 0; }
        }

        /// <summary>
        /// Create a new timer but do not start it yet
        /// </summary>
        /// <param name="duration">base duration</param>
        public Timer(float duration = 0)
        {
            targetTime = duration;
        }
        
        /// <summary>
        /// Start timer with a specified duration
        /// </summary>
        /// <param name="duration">new timer duration</param>
        public void Start(float duration)
        {
            targetTime = duration;
            Start();
        }

        /// <summary>
        /// (re)Start the timer with the current configured duration
        /// </summary>
        public void Start()
        {
            currentTime = 0;
            running = true;
        }

        /// <summary>
        /// Stop the timer
        /// </summary>
        public void Stop()
        {
            running = false;
        }

        /// <summary>
        /// Advance the timer, call this on Update()
        /// When the timer ends it will stop automaticaly
        /// </summary>
        /// <returns>true when the timer ended</returns>
        public bool Tick()
        {
            if (running)
            {
                currentTime += Time.deltaTime;
                if (currentTime >= targetTime )
                {
                    Stop();
                    return true;
                }
            }
            return false;
        }
    }

}