﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Poultry
{

    public class DragNDropTarget : MonoBehaviour, IDropHandler
    {

        private void Start()
        {
            foreach (DragNDroppable draggable in GetComponentsInChildren<DragNDroppable>() )
            {
                draggable.CurrentTarget = this;
            }
        }

        public delegate void DragNDropHandler(DragNDroppable dropped, DragNDropTarget source, DragNDropTarget destination);

        public event DragNDropHandler DragNDropEvent;

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData == null || eventData.pointerDrag == null)
            {
                return;
            }
            DragNDroppable dropped = eventData.pointerDrag.GetComponent<DragNDroppable>();
            if (dropped != null)
            {
                DragNDropEvent?.Invoke(dropped, dropped.CurrentTarget, this);
                dropped.CurrentTarget = this;
            }
        }
    }

}