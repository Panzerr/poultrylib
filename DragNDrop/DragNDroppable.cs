﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Poultry
{
    public class DragNDroppable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    { 
        private Vector3 dragDelta;
        public Vector3 PosBeforeDrag { get; private set; }

        private DragNDropTarget currentTarget;

        public DragNDropTarget CurrentTarget
        {
            get { return currentTarget; }
            set { currentTarget = value; hasANewTarget = true; }
        }

        private bool hasBeenDragged = false;
        private bool hasANewTarget = false;

        public void OnBeginDrag(PointerEventData eventData)
        {
            dragDelta = transform.position - new Vector3(eventData.position.x, eventData.position.y);
            PosBeforeDrag = transform.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = new Vector3(eventData.position.x, eventData.position.y) + dragDelta;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            hasBeenDragged = true;
        }

        private void LateUpdate()
        {
            if (hasBeenDragged && !hasANewTarget)
                transform.position = PosBeforeDrag;
            hasBeenDragged = false;
            hasANewTarget = false;
        }

    }
}