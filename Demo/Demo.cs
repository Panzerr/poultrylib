﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Poultry
{
    //triggers a series of actions for manual testing/tweaking, possibly in a loop
    public class Demo : MonoBehaviour
    {
        [System.Serializable]
        public struct ScheduledAction
        {
            public EventTrigger.TriggerEvent action;
            public float delay;
        }

        [SerializeField]
        private List<ScheduledAction> actions;

        [SerializeField]
        private float loopCooldown = 20f;

        [SerializeField]
        private int numberOfRuns = -1;

        [SerializeField] bool looping;
        
        private int index = 0;

        private float elapsedCooldown = 0f;

        // Update is called once per frame
        void Update()
        {
            if (actions != null && ShouldKeepPlaying())
            {
                if (elapsedCooldown <= 0f)
                {
                    NextAction();
                }
                else
                {
                    elapsedCooldown -= Time.deltaTime;
                }
            }
        }

        bool ShouldKeepPlaying()
        {
            return looping || numberOfRuns > 0;
        }

        void NextAction()
        {
            if (index == actions.Count)
            {
                index = 0;
                elapsedCooldown = loopCooldown;
                if (numberOfRuns > 0)
                    numberOfRuns--;
            } 
            else
            {
                actions[index].action.Invoke(new BaseEventData(EventSystem.current));
                elapsedCooldown = actions[index].delay;
                index++;
            }
        }
    }

}