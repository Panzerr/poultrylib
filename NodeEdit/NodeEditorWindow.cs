﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR

namespace Poultry
{

    public class NodeEditorWindow : EditorWindow
    {
        [MenuItem("Window/Node Window")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(NodeEditorWindow));
        }

        NodeSubWindow subWindow;

        bool isFirsInstanciation;

        NodeObject currentObject;

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        Vector2 scrollPos;

        void OnGUI()
        {
            scrollPos = GUILayout.BeginScrollView(scrollPos,true,true);
            BeginWindows();
            if (IsNode(Selection.activeObject))
            {
                isFirsInstanciation = (currentObject != Selection.activeObject as NodeObject);
                if (isFirsInstanciation)
                {
                    currentObject = Selection.activeObject as NodeObject;
                    isFirsInstanciation = true;
                    subWindow = new NodeSubWindow(new Rect(10,10,200,200),currentObject);
                }
            }
            if (subWindow != null)
                subWindow.OnGUI();

            EndWindows();
            GUILayout.EndScrollView();
        }

        bool IsNode(Object obj)
        {
            return obj != null && (obj.GetType() == typeof(NodeObject) || obj.GetType().IsSubclassOf(typeof(NodeObject)));
        }

    }
}

#endif