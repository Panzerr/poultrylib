﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeObject : ScriptableObject
{
    public List<NodeObject> next;
}
