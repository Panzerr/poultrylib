﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

#if UNITY_EDITOR

public class NodeSubWindow
{
    NodeObject nodeObject;

    Rect winRect;
    public Rect WinRect
    {
        get { return winRect; }        
    }

    static int globalId = 0;

    int id;

    List<NodeObject> nodeObjects = null;

    List<NodeSubWindow> nodeSubWindows = new List<NodeSubWindow>();

    float maxY; //used for placing

    public float MaxY { get { return maxY; } }

    public NodeSubWindow(Rect rect, NodeObject node)
    {
        globalId++;
        id = globalId;
        winRect = rect;
        maxY = rect.y;
        nodeObject = node;
        if (node.next != null)
            nodeObjects = new List<NodeObject>(node.next);
        else
            nodeObjects = new List<NodeObject>();
        FillSubWindows();
    }

    private void FillSubWindows()
    {
        nodeSubWindows.Clear();
        int i = 0;
        foreach(NodeObject it in nodeObjects)
        {
            if (it != null)
            {
                Rect nextRect = new Rect(winRect.x + 250, maxY, winRect.width, winRect.height);
                NodeSubWindow subWindow = new NodeSubWindow(nextRect, it);
                nodeSubWindows.Add(subWindow);
                i++;
                maxY = Mathf.Max(subWindow.MaxY, maxY) + 200;
            }
        }
    }

    public void OnGUI()
    { 
        if (nodeObject.next != null && !nodeObjects.SequenceEqual(nodeObject.next))
        {
            nodeObjects.Clear();
            nodeObjects.AddRange(nodeObject.next);
            FillSubWindows();
        }
        winRect = GUILayout.Window(id, winRect, DoWindow, nodeObject.name);
        foreach(NodeSubWindow subWindow in nodeSubWindows)
        {
            Handles.DrawLine(winRect.center + new Vector2(winRect.width / 2, 0), subWindow.WinRect.center - new Vector2(subWindow.WinRect.width / 2, 0));
            subWindow.OnGUI();
        }
    }

    void DoWindow(int unusedWindowID)
    {
        Editor editor = Editor.CreateEditor(nodeObject);
        editor.DrawDefaultInspector();
        GUI.DragWindow();
    }
    

}

#endif