﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry {
    public class Variant<X, Y>
    {
        private X valueX;
        private Y valueY;

        private enum TypeEnum
        {
            None,
            X,
            Y
        };

        private TypeEnum currentType = TypeEnum.None;

        public Variant(X val)
        {
            valueX = val;
            currentType = TypeEnum.X;
        }

        public Variant(Y val)
        {
            valueY = val;
            currentType = TypeEnum.Y;
        }

        public bool TryGet(out X ret)
        {
            ret = valueX;
            return currentType == TypeEnum.X;
        }
        public bool TryGet(out Y ret)
        {
            ret = valueY;
            return currentType == TypeEnum.Y;
        }
    }
}