﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry
{

    public class Poolable : MonoBehaviour, IInitializable<Pool>
    {

        private Pool pool;

        bool deactivate = false;

        public void InitScript(Pool param_A)
        {
            gameObject.SetActive(false);
            pool = param_A;
        }
        
        /// <summary>
        /// Return the object to the pool
        /// </summary>
        public void Remove()
        {
            transform.SetParent(pool.transform);
            deactivate = true;
            pool.Return(gameObject);
        }

        public void Update()
        {
            if (deactivate)
            {
                gameObject.SetActive(false);
                deactivate = false;
            }
        }
    }
}