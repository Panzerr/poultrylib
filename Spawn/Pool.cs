﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry {

    /// <summary>
    /// Pooling Implementation
    /// </summary>
    public class Pool : MonoBehaviour
    {


        /// <summary>
        /// The actual pool
        /// </summary>
        Stack<GameObject> pool;

        [SerializeField]
        int preloadQuantity = 0;

        [SerializeField]
        GameObject prefab;

        Vector3 prefabPos;

        Quaternion prefabRotate;

        /// <summary>
        /// Initialize the pool
        /// </summary>
        private void Start()
        {
            pool = new Stack<GameObject>();
            prefabPos = prefab.transform.localPosition;
            prefabRotate = prefab.transform.localRotation;
            for (int i = 0; i < preloadQuantity; i++)
            {
                LoadToPool();
            }
        }
        

        /// <summary>
        /// Add one element to pool
        /// </summary>
        private void LoadToPool()
        {
            pool.Push(Spawn<Poolable,Pool>.AsChildPrefab(this,prefab,transform).gameObject);
        }

        /// <summary>
        /// Get an element from the pool
        /// </summary>
        /// <returns>the element in question</returns>
        public GameObject Get()
        {
            if (pool.Count == 0)
            {
                LoadToPool();
                Debug.Log("load");
            }
            return pool.Pop();
        }

        /// <summary>
        /// Return an element to the pool
        /// </summary>
        /// <param name="toReturn"></param>
        public void Return(GameObject toReturn)
        {
            pool.Push(toReturn);
        }

    }

}