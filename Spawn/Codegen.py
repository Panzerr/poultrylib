
code_parameters = ["A", "B", "C", "D"]
param_number = ["first", "second", "third", "fourth"]

test_str = "blabla $args_chevrons, machin $args_doc , bidule ($args_parameters_wcomaT truc), machin($args_parameters) , use($args_listbase), use($args_list_wcoma base); truc < T$args_generic > "

with open("./IInitializable_template.txt", 'r') as file:
    iintializable_str = file.read()

with open("./Spawn_template.txt", 'r') as file:
    spawn_str = file.read()


def genericize(inputstr):
    result = "//This file was generated, please do not modify it directly\nusing System.Collections;\nusing System.Collections.Generic;\nusing UnityEngine;\n\nnamespace Poultry\n{"

    for i in range(4):
        args_chevrons = ""
        if i != 0:
            args_chevrons = "<"
        args_doc = ""
        args_parameters = ""
        args_list = ""
        args_generic = ""
        types_doc = ""
        for j in range(i):

            args_chevrons += code_parameters[j]
            args_parameters += code_parameters[j] + " param_" + code_parameters[j]
            args_generic += ", " + code_parameters[j]
            args_doc += "\n        ///<param name=\"param_" + code_parameters[j] + "\"> " + param_number[j] + " parameter </param>"
            types_doc += "\n        ///<typeparam name=\""+code_parameters[j]+"\"> " + param_number[j] + " parameter type </param>"
            args_list += "param_" + code_parameters[j]
            if j != i-1:
                args_chevrons += ', '
                args_parameters += ', '
                args_list += ", "

        args_parameters_wcoma = args_parameters
        args_list_wcoma = args_list
        if i != 0:
            args_chevrons += ">"
            args_parameters_wcoma += ", "
            args_list_wcoma += ", "

        toPrint = inputstr.replace('$args_chevrons', args_chevrons)
        toPrint = toPrint.replace('$args_generic', args_generic)
        toPrint = toPrint.replace('$args_doc', args_doc)
        toPrint = toPrint.replace('$args_parameters_wcoma', args_parameters_wcoma)
        toPrint = toPrint.replace('$args_parameters', args_parameters)
        toPrint = toPrint.replace('$args_list_wcoma', args_list_wcoma)
        toPrint = toPrint.replace('$args_list', args_list)
        result += "\n" + toPrint
    result += "\n}"
    return result

with open("IInitializable.cs",'w') as file:
    file.write(genericize(iintializable_str))

with open("Spawn.cs",'w') as file:
    file.write(genericize(spawn_str))


