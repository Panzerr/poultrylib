//This file was generated, please do not modify it directly
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry
{
    /// <summary>
    /// Functions to Spawn behaviour and intitialize them without parameters
    /// </summary>
    /// <typeparam name="T"> The object to be created, must implement IInitializable </typeparam>
    public static class Spawn<T> where T : MonoBehaviour, IInitializable
    {

        /// <summary>
        /// Spawn in a new empty gameobject
        /// </summary>
        /// <param name="objectName"> name of the created object </param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsObject(string objectName)
        {
            T component = new GameObject(objectName).AddComponent<T>();
            component.InitScript();
            return component;
        }

        /// <summary>
        /// Spawn in a prefab, this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsPrefab
        /// </summary>
        /// <param name="prefab">The prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsPrefab(GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return AsComponent(gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsChildPrefab
        /// </summary>
        /// <param name="prefab">The prefab to use</param>
        /// <param name="parent">The future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsChildPrefab(GameObject prefab, Transform parent)
        {
            GameObject gameObject = GameObject.Instantiate(prefab, parent);
            return AsComponent(gameObject);
        }

        /// <summary>
        /// Spawn in a prefab, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        /// <param name="prefab">the prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsPrefab(GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return UniqueAsComponent(gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        /// <param name="prefab">the prefab to use</param>
        /// <param name="parent">the future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsChildPrefab(GameObject prefab, Transform parent)
        {
            T component = parent.GetComponentInChildren<T>();
            if (component == null)
                return AsChildPrefab(prefab, parent);
            else
                component.InitScript();
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject,  this version WILL spawn a second instance of the component if there is already one there
        /// for unicity please see UniqueAsComponent
        /// </summary>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsComponent(GameObject gameObject)
        {
            T component = gameObject.AddComponent<T>();
            component.InitScript();
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject, if the GameObject already contains the component this only initialize the existing one
        /// </summary>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsComponent(GameObject gameObject)
        {
            T component = gameObject.GetComponent<T>();
            if (component == null)
                component = gameObject.AddComponent<T>();
            component.InitScript();
            return component;
        }

    }

    /// <summary>
    /// Functions to Spawn behaviour and intitialize them without parameters
    /// </summary>
    /// <typeparam name="T"> The object to be created, must implement IInitializable </typeparam>
    public static class Spawn<T, A> where T : MonoBehaviour, IInitializable<A>
    {

        /// <summary>
        /// Spawn in a new empty gameobject
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="objectName"> name of the created object </param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsObject(A param_A, string objectName)
        {
            T component = new GameObject(objectName).AddComponent<T>();
            component.InitScript(param_A);
            return component;
        }

        /// <summary>
        /// Spawn in a prefab, this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsPrefab
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="prefab">The prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsPrefab(A param_A, GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return AsComponent(param_A, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsChildPrefab
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="prefab">The prefab to use</param>
        /// <param name="parent">The future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsChildPrefab(A param_A, GameObject prefab, Transform parent)
        {
            GameObject gameObject = GameObject.Instantiate(prefab, parent);
            return AsComponent(param_A, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="prefab">the prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsPrefab(A param_A, GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return UniqueAsComponent(param_A, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="prefab">the prefab to use</param>
        /// <param name="parent">the future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsChildPrefab(A param_A, GameObject prefab, Transform parent)
        {
            T component = parent.GetComponentInChildren<T>();
            if (component == null)
                return AsChildPrefab(param_A, prefab, parent);
            else
                component.InitScript(param_A);
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject,  this version WILL spawn a second instance of the component if there is already one there
        /// for unicity please see UniqueAsComponent
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsComponent(A param_A, GameObject gameObject)
        {
            T component = gameObject.AddComponent<T>();
            component.InitScript(param_A);
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject, if the GameObject already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsComponent(A param_A, GameObject gameObject)
        {
            T component = gameObject.GetComponent<T>();
            if (component == null)
                component = gameObject.AddComponent<T>();
            component.InitScript(param_A);
            return component;
        }

    }

    /// <summary>
    /// Functions to Spawn behaviour and intitialize them without parameters
    /// </summary>
    /// <typeparam name="T"> The object to be created, must implement IInitializable </typeparam>
    public static class Spawn<T, A, B> where T : MonoBehaviour, IInitializable<A, B>
    {

        /// <summary>
        /// Spawn in a new empty gameobject
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="objectName"> name of the created object </param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsObject(A param_A, B param_B, string objectName)
        {
            T component = new GameObject(objectName).AddComponent<T>();
            component.InitScript(param_A, param_B);
            return component;
        }

        /// <summary>
        /// Spawn in a prefab, this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsPrefab
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="prefab">The prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsPrefab(A param_A, B param_B, GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return AsComponent(param_A, param_B, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsChildPrefab
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="prefab">The prefab to use</param>
        /// <param name="parent">The future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsChildPrefab(A param_A, B param_B, GameObject prefab, Transform parent)
        {
            GameObject gameObject = GameObject.Instantiate(prefab, parent);
            return AsComponent(param_A, param_B, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="prefab">the prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsPrefab(A param_A, B param_B, GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return UniqueAsComponent(param_A, param_B, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="prefab">the prefab to use</param>
        /// <param name="parent">the future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsChildPrefab(A param_A, B param_B, GameObject prefab, Transform parent)
        {
            T component = parent.GetComponentInChildren<T>();
            if (component == null)
                return AsChildPrefab(param_A, param_B, prefab, parent);
            else
                component.InitScript(param_A, param_B);
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject,  this version WILL spawn a second instance of the component if there is already one there
        /// for unicity please see UniqueAsComponent
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsComponent(A param_A, B param_B, GameObject gameObject)
        {
            T component = gameObject.AddComponent<T>();
            component.InitScript(param_A, param_B);
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject, if the GameObject already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsComponent(A param_A, B param_B, GameObject gameObject)
        {
            T component = gameObject.GetComponent<T>();
            if (component == null)
                component = gameObject.AddComponent<T>();
            component.InitScript(param_A, param_B);
            return component;
        }

    }

    /// <summary>
    /// Functions to Spawn behaviour and intitialize them without parameters
    /// </summary>
    /// <typeparam name="T"> The object to be created, must implement IInitializable </typeparam>
    public static class Spawn<T, A, B, C> where T : MonoBehaviour, IInitializable<A, B, C>
    {

        /// <summary>
        /// Spawn in a new empty gameobject
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="objectName"> name of the created object </param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsObject(A param_A, B param_B, C param_C, string objectName)
        {
            T component = new GameObject(objectName).AddComponent<T>();
            component.InitScript(param_A, param_B, param_C);
            return component;
        }

        /// <summary>
        /// Spawn in a prefab, this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsPrefab
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="prefab">The prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsPrefab(A param_A, B param_B, C param_C, GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return AsComponent(param_A, param_B, param_C, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent this version WILL spawn a second instance of the component if there is already one there,
        /// for unicity of the component created, please see UniqueAsChildPrefab
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="prefab">The prefab to use</param>
        /// <param name="parent">The future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsChildPrefab(A param_A, B param_B, C param_C, GameObject prefab, Transform parent)
        {
            GameObject gameObject = GameObject.Instantiate(prefab, parent);
            return AsComponent(param_A, param_B, param_C, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="prefab">the prefab to use</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsPrefab(A param_A, B param_B, C param_C, GameObject prefab)
        {
            GameObject gameObject = GameObject.Instantiate(prefab);
            return UniqueAsComponent(param_A, param_B, param_C, gameObject);
        }

        /// <summary>
        /// Spawn in a prefab and set its parent, if the prefab already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="prefab">the prefab to use</param>
        /// <param name="parent">the future parent of the prefab</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsChildPrefab(A param_A, B param_B, C param_C, GameObject prefab, Transform parent)
        {
            T component = parent.GetComponentInChildren<T>();
            if (component == null)
                return AsChildPrefab(param_A, param_B, param_C, prefab, parent);
            else
                component.InitScript(param_A, param_B, param_C);
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject,  this version WILL spawn a second instance of the component if there is already one there
        /// for unicity please see UniqueAsComponent
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T AsComponent(A param_A, B param_B, C param_C, GameObject gameObject)
        {
            T component = gameObject.AddComponent<T>();
            component.InitScript(param_A, param_B, param_C);
            return component;
        }

        /// <summary>
        /// Spawn as a component of an existing GameObject, if the GameObject already contains the component this only initialize the existing one
        /// </summary>
        ///<param name="param_A"> first parameter </param>
        ///<param name="param_B"> second parameter </param>
        ///<param name="param_C"> third parameter </param>
        /// <param name="gameObject">the target GameObject</param>
        /// <returns>reference to the instanciated component</returns>
        static public T UniqueAsComponent(A param_A, B param_B, C param_C, GameObject gameObject)
        {
            T component = gameObject.GetComponent<T>();
            if (component == null)
                component = gameObject.AddComponent<T>();
            component.InitScript(param_A, param_B, param_C);
            return component;
        }

    }

}