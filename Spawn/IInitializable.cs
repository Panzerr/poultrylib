//This file was generated, please do not modify it directly
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poultry
{

    public interface IInitializable
    {
        void InitScript();
    }


    public interface IInitializable<A>
    {
        void InitScript(A param_A);
    }


    public interface IInitializable<A, B>
    {
        void InitScript(A param_A, B param_B);
    }


    public interface IInitializable<A, B, C>
    {
        void InitScript(A param_A, B param_B, C param_C);
    }

}