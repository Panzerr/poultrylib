﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Poultry
{ 
    /**
    * This MonoBehaviour is used to play sounds with randomized pitch and volume each time.
    * In video games the primary use for this is for footsteps, but you could use it for
    * any repeated sound such as gunfire or water dripping. 
    */
    [RequireComponent(typeof(AudioSource))]
    public class FootstepsPlayer : MonoBehaviour
    {
        [Tooltip("The list of sound clip the player will be choosing from at random")] [SerializeField] AudioClip[] soundbites;
        [Range(0, 3)] [Tooltip("Increase this to get wider pitch variations")] [SerializeField] private float randomPitchRange;
        [Range(0, 1f)] [Tooltip("Increase this to get wider volume variations")]  [SerializeField] private float randomVolumeRange;

        [SerializeField] [Header("Loop")] private bool looping;
        [SerializeField] private float loopInterval;

        private AudioSource audioSource;
        private float basePitch;
        private float baseVolume;
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            basePitch = audioSource.pitch;
            baseVolume = audioSource.volume;
        }

        /**
        * Play a random sound from the sound list with randomized pitch and volume
        */
        public void PlayRandomOneShot()
        {
            audioSource.Stop();
            if (soundbites.Length == 0) return;

            audioSource.pitch = basePitch;
            audioSource.volume = baseVolume;

            float randomPitch = basePitch + Random.Range(-randomPitchRange, +randomPitchRange);
            float randomVolume = baseVolume + Random.Range(-randomVolumeRange, +randomVolumeRange);

            audioSource.pitch = randomPitch;
            audioSource.volume = randomVolume;

            audioSource.PlayOneShot(GetRandomSoundbite());
        }

        /**
         * Start playing random sounds in a loop
         */
        public void StartLoop()
        {
            looping = true;
        }

        /**
         * Stop the loop
         */
        public void StopLoop()
        {
            looping = false;
        }
        
        private float timeToNextSound;
        void Update()
        {
            if (!looping) return;

            timeToNextSound -= Time.deltaTime;
            if (timeToNextSound <= 0)
            {
                timeToNextSound = loopInterval;
                PlayRandomOneShot();
            }
        }

        private AudioClip GetRandomSoundbite()
        {
            int range = soundbites.Length;
            int randomIndex = Random.Range(0, range);
            return soundbites[randomIndex];
        }

    }
}